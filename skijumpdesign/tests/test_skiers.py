import os
from math import isclose

import numpy as np
import matplotlib.pyplot as plt

from ..skiers import Skier
from ..surfaces import Surface, FlatSurface, TakeoffSurface
from ..utils import vel2speed


def test_skier(plot=False):

    mass = 75.0
    area = 0.5
    drag_coeff = 1.0
    friction_coeff = 0.3
    air_density = 0.85

    skier = Skier(mass, area, drag_coeff, friction_coeff)

    assert isclose(skier.mass, mass)
    assert isclose(skier.area, area)
    assert isclose(skier.drag_coeff, drag_coeff)
    assert isclose(skier.friction_coeff, friction_coeff)

    vel = -10.0

    assert isclose(skier.drag_force(vel), 1 / 2 * vel**2 * air_density *
                   drag_coeff * area)

    assert isclose(skier.friction_force(vel, slope=10.0),
                   friction_coeff * mass * 9.81 * np.cos(np.tan(10.0)))

    takeoff_pos = (4.0, 3.0)  # x, y
    takeoff_vel = (1.0, 10.0)  # vx, vy

    surf = Surface(np.linspace(0.0, 10.0, num=10), np.zeros(10))

    flight_traj = skier.fly_to(surf, takeoff_pos, takeoff_vel)

    if plot:
        ax = surf.plot()
        flight_traj.plot(ax=ax)

    landing_pos = tuple(flight_traj.pos[-1])
    landing_vel = tuple(flight_traj.vel[-1])

    takeoff_speed, takeoff_angle = vel2speed(*takeoff_vel)

    takeoff_speed2, landing_vel2 = skier.speed_to_land_at(landing_pos,
                                                          takeoff_pos,
                                                          takeoff_angle,
                                                          surf=surf)

    assert isclose(takeoff_speed, takeoff_speed2, rel_tol=1e-5)
    assert isclose(landing_vel[0], landing_vel2[0], rel_tol=1e-5)
    assert isclose(landing_vel[1], landing_vel2[1], rel_tol=1e-5)


def test_slide_on():

    x = np.linspace(0, 20)
    y = -1.0 * x + 10.0

    x = np.linspace(0, 6 * np.pi)
    y = np.sin(x)

    surf = Surface(x, y)

    mass = 75.0
    area = 0.5
    drag_coeff = 1.0
    friction_coeff = 0.3

    skier = Skier(mass, area, drag_coeff, friction_coeff)

    traj = skier.slide_on(surf, 50.0)

    expected_times = \
    np.array([0.        , 0.00280244, 0.00560489, 0.00840733, 0.01120978,
              0.01401222, 0.01681466, 0.01961711, 0.02241955, 0.025222  ,
              0.02802444, 0.03082689, 0.03362933, 0.03643177, 0.03923422,
              0.04203666, 0.04483911, 0.04764155, 0.05044399, 0.05324644,
              0.05604888, 0.05885133, 0.06165377, 0.06445622, 0.06725866,
              0.0700611 , 0.07286355, 0.07566599, 0.07846844, 0.08127088,
              0.08407332, 0.08687577, 0.08967821, 0.09248066, 0.0952831 ,
              0.09808555, 0.10088799, 0.10369043, 0.10649288, 0.10929532,
              0.11209777, 0.11490021, 0.11770265, 0.1205051 , 0.12330754,
              0.12610999, 0.12891243, 0.13171488, 0.13451732, 0.13731976,
              0.14012221, 0.14292465, 0.1457271 , 0.14852954, 0.15133198,
              0.15413443, 0.15693687, 0.15973932, 0.16254176, 0.16534421,
              0.16814665, 0.17094909, 0.17375154, 0.17655398, 0.17935643,
              0.18215887, 0.18496131, 0.18776376, 0.1905662 , 0.19336865,
              0.19617109, 0.19897353, 0.20177598, 0.20457842, 0.20738087,
              0.21018331, 0.21298576, 0.2157882 , 0.21859064, 0.22139309,
              0.22419553, 0.22699798, 0.22980042, 0.23260286, 0.23540531,
              0.23820775, 0.2410102 , 0.24381264, 0.24661509, 0.24941753,
              0.25221997, 0.25502242, 0.25782486, 0.26062731, 0.26342975,
              0.26623219, 0.26903464, 0.27183708, 0.27463953, 0.27744197,
              0.28024442, 0.28304686, 0.2858493 , 0.28865175, 0.29145419,
              0.29425664, 0.29705908, 0.29986152, 0.30266397, 0.30546641,
              0.30826886, 0.3110713 , 0.31387375, 0.31667619, 0.31947863,
              0.32228108, 0.32508352, 0.32788597, 0.33068841, 0.33349085,
              0.3362933 , 0.33909574, 0.34189819, 0.34470063, 0.34750308,
              0.35030552, 0.35310796, 0.35591041, 0.35871285, 0.3615153 ,
              0.36431774, 0.36712018, 0.36992263, 0.37272507, 0.37552752,
              0.37832996])

    expected_x = \
        np.array([0.        ,  0.09784475,  0.19777525,  0.29992847,  0.40491869,
                  0.51348864,  0.62650421,  0.74495439,  0.86995133,  1.0027303 ,
                  1.14464973,  1.29719115,  1.46189005,  1.63726295,  1.81848264,
                  2.00142598,  2.18291082,  2.36069608,  2.53348168,  2.70090855,
                  2.86355868,  3.02295506,  3.1815617 ,  3.34120674,  3.5025136 ,
                  3.66752315,  3.83735082,  4.01214999,  4.19111199,  4.3724661 ,
                  4.55347958,  4.73045761,  4.89874334,  5.0541556 ,  5.19814981,
                  5.33212431,  5.45734583,  5.57508074,  5.68659503,  5.79315431,
                  5.89602384,  5.99646849,  6.09575277,  6.19474951,  6.29314269,
                  6.39139911,  6.4901385 ,  6.59006016,  6.69194295,  6.79664528,
                  6.90510515,  7.0183401 ,  7.13744724,  7.26360323,  7.39806431,
                  7.54216627,  7.69732517,  7.86385955,  8.03770472,  8.21450149,
                  8.39082193,  8.56416935,  8.73297827,  8.89661445,  9.05537489,
                  9.21048782,  9.36411207,  9.51745677,  9.67152247,  9.82816513,
                  9.98857423, 10.15327286, 10.32211767, 10.49429886, 10.66834022,
                  10.84209912, 11.01276647, 11.17538059, 11.32738739, 11.46953331,
                  11.60264286, 11.72761685, 11.84543233, 11.95714262, 12.06387729,
                  12.16684219, 12.26729003, 12.36599485, 12.46340207, 12.56001902,
                  12.65640917, 12.75319208, 12.85104349, 12.95069522, 13.05293525,
                  13.15860766, 13.26861268, 13.38390666, 13.50550208, 13.63457977,
                  13.77286543, 13.92092581, 14.07832859, 14.24375296, 14.41498961,
                  14.58894073, 14.76162001, 14.92847904, 15.08987234, 15.24712878,
                  15.40096057, 15.55224253, 15.70201217, 15.85146963, 16.00197771,
                  16.15553368, 16.31548681, 16.48000455, 16.64668796, 16.8134352 ,
                  16.97844153, 17.14019932, 17.29749805, 17.4494243 , 17.59536175,
                  17.73499118, 17.8682905 , 17.9955347 , 18.1172275 , 18.23344923,
                  18.3448405 , 18.45213665, 18.55595889, 18.65681429, 18.75509577,
                  18.8510821 ])

    expected_v = \
        np.array([50.        , 50.18720157, 50.42061085, 50.70336528, 51.05264135,
                  51.49401006, 52.06133997, 52.79679724, 53.75084559, 54.98224636,
                  56.55805845, 58.55363835, 61.05280012, 63.98474308, 67.06630747,
                  70.05123618, 72.74734174, 75.01650637, 76.77468193, 77.99188995,
                  78.69222162, 78.95383778, 78.90896895, 78.73217339, 78.33222455,
                  77.48730127, 76.0601154 , 73.99934888, 71.33965385, 68.20165252,
                  64.7919373 , 61.40307068, 58.41358534, 56.12126294, 54.29404897,
                  52.85149452, 51.73589499, 50.89298001, 50.2719134 , 49.82529317,
                  49.50915155, 49.28295497, 49.10960404, 48.97726857, 48.92722267,
                  48.94820634, 49.03301761, 49.18329918, 49.40953837, 49.73106715,
                  50.17606214, 50.78154462, 51.59338047, 52.66628026, 54.06379917,
                  55.85833706, 58.130934  , 60.84614632, 63.74177437, 66.58494749,
                  69.19293358, 71.4331391 , 73.22310904, 74.53052697, 75.37321501,
                  75.81913381, 75.98638367, 75.98331994, 75.7234165 , 75.10016694,
                  74.04688383, 72.53669866, 70.5825619 , 68.23724294, 65.59333013,
                  62.78323077, 59.97917111, 57.4494646 , 55.31834263, 53.55285865,
                  52.11788272, 50.97618426, 50.08843206, 49.41319427, 48.90693842,
                  48.52403138, 48.22100934, 47.99835539, 47.85114441, 47.77204326,
                  47.75838488, 47.81216832, 47.94005875, 48.15338742, 48.46815173,
                  48.90501513, 49.48930723, 50.2510237 , 51.22482635, 52.45292996,
                  54.01047351, 55.94904225, 58.25843882, 60.86680224, 63.6406079 ,
                  66.38466761, 68.84212952, 70.71969469, 72.09806602, 73.10070262,
                  73.77631088, 74.1607385 , 74.2769744 , 74.13514879, 73.73253315,
                  73.0006951 , 71.64119007, 69.79929542, 67.67434969, 65.43430368,
                  63.2157205 , 61.12377558, 59.23225661, 57.5835636 , 56.18870885,
                  55.02731698, 54.04762486, 53.1664817 , 52.28927089, 51.52774432,
                  50.90226601, 50.38589572, 49.95644903, 49.59649735, 49.29336792,
                  49.03914381])

    np.testing.assert_allclose(expected_times, traj.t, rtol=1e-5)
    np.testing.assert_allclose(expected_x, traj.pos[:, 0], rtol=1e-5)
    np.testing.assert_allclose(expected_v, traj.speed, rtol=1e-5)


def test_slide_on_regression():

    approach_ang = -np.deg2rad(20)  # radians
    approach_len = 20.0  # meters
    takeoff_ang = np.deg2rad(15)

    skier = Skier()

    approach = FlatSurface(approach_ang, approach_len)

    takeoff_entry_speed = skier.end_speed_on(approach)

    expected_speed = 10.92727081007988054750512674218044

    assert isclose(takeoff_entry_speed, expected_speed, rel_tol=1e-16)

    takeoff_entry_vel = skier.end_vel_on(approach)

    # NOTE : Not true regression here because there was a bug in end_vel_on
    # which used tan instead of arctan.
    expected_vx = 10.26827574556135758143682323861867
    expected_vy = -3.7373467286218100547046105930348

    assert isclose(takeoff_entry_vel[0], expected_vx)
    assert isclose(takeoff_entry_vel[1], expected_vy)

    takeoff = TakeoffSurface(skier, approach_ang, takeoff_ang, expected_speed,
                             init_pos=approach.end)

    takeoff_traj = skier.slide_on(takeoff, expected_speed)

    this_dir = os.path.dirname(os.path.realpath(__file__))
    expected_times = np.loadtxt(os.path.join(this_dir, 'slide-on-times.txt'),
                                delimiter=',')
    expected_traj = np.loadtxt(os.path.join(this_dir, 'slide-on-traj.txt'),
                               delimiter=',')

    np.testing.assert_allclose(takeoff_traj.t,
                               expected_times, rtol=1e-14)
    np.testing.assert_allclose(takeoff_traj.pos[:, 0],
                               expected_traj[0], rtol=1e-15)
    np.testing.assert_allclose(takeoff_traj.speed,
                               expected_traj[1], rtol=1e-14)
